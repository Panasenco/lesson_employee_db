const UI = require("./ui.js");

let ui = new UI();

ui.on('addEmployee', (name, age, managerID)=>{
    return 0; //todo: should return new index
});

ui.on("allEmployee", ()=>{
    return ""; //todo: should return string with all users
});

ui.on('countSubordinate', (id)=>{
    return 0; //todo: should return number
});

ui.on('listSubordinate', (id)=>{
    return ""; //todo: should return string with subordinate users
});

ui.on('isSubordinate', function(managerId, employeeId){
    return false; //todo: should return boolean
});

ui.start();
