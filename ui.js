const prompt = require('prompt');

function getListAction(){
    return ` SELECT AN ACTION
        1) add employee
        2) show employee list
        3) count of subordinate
        4) show list of subordinate
        5) is subordinate
        6) exit
    `;
}

module.exports = class UI {
    constructor() {
        console.log("new UI");
        this._listeners = {};
    }

    start(){
        prompt.start();
        this._loop();
    }

    _loop(){
        console.log(getListAction());
        prompt.get(['actionCode'], (err1, result1)=> {
            console.clear();
            let res = "";
            switch (result1.actionCode) {
                case "1":
                    prompt.get(['name', 'age', 'managerId'], (err, result)=>{
                        if (err) {
                            console.error(err);
                        }else{
                            console.log(this._notify("addEmployee", result.name, result.age, result.managerId));
                        }
                        this._loop();
                    });
                    break;
                case "2":
                    console.log(this._notify("allEmployee"));
                    this._loop();
                    break;
                case "3":
                    prompt.get(['managerId'], (err, result)=>{
                        if (err) {
                            console.error(err);
                        }else{
                            console.log(this._notify("countSubordinate", result.managerId));
                        }
                        this._loop();
                    });
                    break;
                case "4":
                    prompt.get(['managerId'], (err, result)=>{
                        if (err) {
                            console.error(err);
                        }else{
                            console.log(this._notify("listSubordinate", result.managerId));
                        }
                        this._loop();
                    });
                    break;
                case "5":
                    prompt.get(['managerId', 'employeeId'], (err, result)=>{
                        if (err) {
                            console.error(err);
                        }else{
                            console.log(this._notify("isSubordinate", result.managerId, result.employeeId));
                        }
                        this._loop();
                    });
                    break;
                case "6":
                    console.clear();
                    return;
                default:
                    console.log("Oop!!");
                    this._loop();
            }
        });
    }

    _notify(name, ...args){
        for(let key of Object.keys(this._listeners)){
            if(key==name){
                let res = "";
                for(let listener of this._listeners[key]){
                    res+=listener(...args)+"\n";
                }
                return res;
            }
        }
        return "action not found";
    }

    on(eventName, eventAction){
        if(!this._listeners[eventName]){
            this._listeners[eventName]=[];
        }
        this._listeners[eventName].push(eventAction);
    }
}
